# Maven Web App Demo

A hello world project to demonstrate Gitlab CI/CD

## Getting Started

Simply git clone the project and add teh appropriate Gitlab CI/CD variables to your project.  

### Prerequisites

This project requires an AWS account and Azure account and an Openshift cluster to deploy a Docker container.  This project also requires a Sonarqube server to run code scans on the source code.


## Authors

* **Richard Murphy** - *Initial work* 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


[![Quality Gate](http://sonar.sophron.io/api/badges/gate?key=HelloWorldASP)](http://sonar.sophron.io/dashboard/index/HelloWorldASP)


